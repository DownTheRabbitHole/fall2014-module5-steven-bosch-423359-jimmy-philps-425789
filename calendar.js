$(document).ready(function(){
    
    $("#logout").hide();
    $("#welcome").hide();
    $("#newCalendarName").hide();
    $("#addCalendarAccept").hide();
    var monthStrings = ["January", "February", "March","April","May","June","July","August","September","October","November","December"];
    var today = new Date();
    var currentMonth = new Month(today.getFullYear(), today.getMonth());
    
    // Session variables.
    var userID ="";
    var token="";
    var userEmail="";
    var enabledCalendars = [];
    calendarInit();
    
    var dialogCalendars = $( "#dialog-calendars" ).dialog({
      autoOpen: false,
      height: 500,
      width: 350,
      modal: true,
      buttons: {
        "Ok": changeCalendars,
        Cancel: function() {
                    dialogCalendars.dialog( "close" );
                }

      }
    });
    
    
    var dialog = $( "#dialog-form" ).dialog({
      autoOpen: false,
      height: 500,
      width: 350,
      modal: true,
      buttons: {
        Cancel: function() {
                    dialog.dialog( "close" );
                }

      }
    });
    
    var registerDialog = $("#dialog-register").dialog({
        autoOpen: false,
        height: 500,
        width: 350,
        modal: true,
        buttons: {
            "Register": validateRegister,
            Cancel: function() {
                registerDialog.dialog("close");
            }
        }
    });
    
    function changeCalendars(){
        if (userID != "" && userEmail != "" && token != "") {
            var calendars = $("#calendarDiv").children("div");
            var calendar = calendars.first();
            for (var i=0; i<calendars.length; i++){
                var calID = calendar.prop('id');
                var view = calendar.children("input").first().prop('checked');
                if (view) {
                    view = 1;
                }else{
                    view = 0;
                }
                $.post("UpdateCalendar.php", {"userID":userID, "token":token, "calendar_id":calID, "view":view}, function(data){
                    }, 'json');
                calendar = calendar.next();
            }
            dialogCalendars.dialog("close");
            generateCalendar(currentMonth);
        }
    }
    function deleteEvent(){
        if (userID != "" && userEmail != "" && token != "") {
            var eventID = $("#eventID").val();
            $.post("ProcessEventDelete.php", {"userID":userID, "token":token, "eventID":eventID}, function(data){
                    dialog.dialog("close");
                    if (data.success == "true") {
                        generateCalendar(currentMonth);
                    }
                    $("#hour").val("1");
                    $("#minute").val("00");
                    $("#ampm").val("AM");
                    $("#eventID").val("");
                    $("#details").val("");
                    $("#event_title").val("");
                    
                }, 'json');
        }
    }
    
    function validateEditEvent() {
        if (userID != "" && userEmail != "" && token != "") {
            var errorMsg = "";
            var eventID = $("#eventID");
            var hour = $("#hour");
            var minute = $("#minute");
            var title = $("#event_title");
            var cell = $("#cell");
            var date = $("#date");
            var day = $("#day");
            var month = $("#month");
            var year = $("#year");
            var details = $("#details");
            var calendar = $("#eventCalendars");
            // Filter Event Title
            var re = /^[a-zA-Z0-9 '&-+$!?]*$/;
            if (errorMsg == "") {
                if (title.val() != "" && title.val() != null) {
                    var exprEvt = title.val().match(re);
                    if (exprEvt) {
                        if (title.val().length > 30) {
                            errorMsg = "Event title is too long.";
                            title.select();
                        }
                    }else{
                        errorMsg = "Event title contains invalid characters.";
                        title.select();
                    }
                }else{
                    errorMsg = "Please enter an event title.";
                    title.select();
                }
            }
            
            //Filter event details
            re = /^[a-zA-Z0-9 .?!()-+=]*$/;
            if (errorMsg == "") {
                if (details.val() != "" && details.val() != null) {
                    var exprDet = details.val().match(re);
                    if (exprDet) {
                        if (details.val().legnth > 500) {
                            errorMsg = "Event details are too long.";
                            details.select();
                        }
                    }else{
                        errorMsg = "Event details contain invalid characters.";
                        details.select();
                    }
                }
            }
            if (errorMsg != "") {
                alert(errorMsg);
            }else{
                // Send to server
                // Month offset
                var monthInt = parseInt(month.val()) + 1;
                var datetime = createDate(minute.val(), hour.val(), day.val(), monthInt, year.val());
                $.post("ProcessEventEdit.php", {"title":title.val(), "cell":cell.val(), "date":datetime,
                       "details":details.val(), "userID":userID, "email":userEmail, "token":token, "eventID":eventID.val(), "calendar_id":calendar.val()}, function(data){
                    dialog.dialog( "close" );
                    title.val("");
                    date.val("");
                    cell.val("");
                    details.val("");
                    if (data.success == "true") {
                        generateCalendar(currentMonth);
                    }
                    hour.val("1");
                    minute.val("00");
                    $("#ampm").val("AM");
                    eventID.val("");
                    details.val("");
                    title.val("");
                }, 'json');
            }
        }
    }
    
    function parseDate(datetime) {
        var twoparts = datetime.split(" ");
        var date = twoparts[0];
        var time = twoparts[1];
        
        var datethreeparts = date.split("-");
        var timethreeparts = time.split(":");
        
        var year = datethreeparts[0];
        var monthInt = parseInt(datethreeparts[1])-1;
        var month = "" + monthInt;
        var day = datethreeparts[2];
        
        var hour = timethreeparts[0];
        var minute = timethreeparts[1];
        //var second = timethreeparts[2];
        
        var array = [year, month, day, hour, minute]; 
        return array;
    }
        
    function loadEvents(){
        // Check user logged in.
        if (userID != "" && userEmail != "" && token != "") {
           retrieveCalendars();
           $.post("RetrieveEvents.php", {"userID":userID, "email":userEmail, "token":token}, function(data){
                    if (data.success == "true") {
                        for (var j=0; j<Object.keys(data).length-1; j++){
                            var eventID = data[j]['eventID'];
                            var retTitle = data[j]['title'];
                            var retDetails = data[j]['details'];
                            var retCalendarId = data[j]['calendar_id'];
                            var retDate = parseDate(data[j]['date']);
                            var retYear = retDate[0];
                            var retMonth = retDate[1];
                            var retDay = retDate[2];
                            var retHour = retDate[3];
                            var retMinute = retDate[4];
                            if (retMonth.substring(0,1) == "0" && retMonth.length > 1) {
                                retMonth = retMonth.substring(1);
                            }                            
                            if (retDay.substring(0,1) == "0") {
                                retDay = retDay.substring(1);
                            }
                            var cellID = retYear + retMonth + retDay;
                            var str = ".mainCell[id=" + cellID+"]";
                            var evtDiv = $("#calendar").find(str).first();
                            var evtDisp = $("<div class='event'></div>");
                            if (retTitle.length >= 14) {
                                retTitle = retTitle.substring(0, 14) + "...";
                            }
                            var ampm = "AM";
                            var parsedHour = parseInt(retHour);
                            if (parsedHour > 12) {
                                parsedHour -= 12;
                                ampm = "PM";
                            }
                            
                            var eventIDField = $("<input type='hidden'></input>").val(eventID);
                            var titleLabel = $("<label class='title'></label>").text(retTitle);
                            var timeLabel = $("<label class='time'></label>").text(parsedHour+":"+retMinute+ampm);
                            evtDisp.append(titleLabel, timeLabel, eventIDField);
                            if (enabledCalendars[retCalendarId] == "1") {
                                evtDisp.show();
                            }else{
                                evtDisp.hide();
                            }
                            evtDiv.append(evtDisp);
                        }
                    }
                }, 'json');
        }
    }
    
    // Login button
    $("#login").click(login);
     // Register button
    $("#register").click(function(){
        registerDialog.dialog("open");
    });
    
    $("#logout").click(logout);
    
    // Previous month button
    $("#prevMonth").click(prevMonth);
        
    // Next month button
    $("#nextMonth").click(nextMonth);

    // Today button
    $("#today").click(displayToday);
    
    // Calendars button
    $("#calendars").click(function(){
        retrieveCalendars();
        dialogCalendars.dialog("open");
    });
    
    function retrieveCalendars(){
         if (userID != "" && userEmail != "" && token != "") {
            $.post("RetrieveCalendars.php", {"userID":userID, "token":token}, function(data){
                if (data.success == "true") {
                    $("#calendarDiv").empty();
                    for (var j=0; j<Object.keys(data).length-1; j++){

                        var calendarID = data[j]['calendar_id'];
                        var view = data[j]['view'];
                        var title = data[j]['title'];
                        // create calendar view
                        var calendarView = $("<div></div>").prop("id", calendarID);
                        var titleView = $("<label class='lblCal'></label>").text(title);
                        var viewCheck = $("<input type='checkbox' class='checkCal'>");
                        if (view == 1) {
                            viewCheck.prop("checked", true);
                        }else{
                            viewCheck.prop("checked", false);
                        }
                        calendarView.append(titleView, viewCheck);
                        $("#calendarDiv").append(calendarView);
                        enabledCalendars[calendarID] = ""+view;
                    }
                }
                
                }, 'json');
        }
    }
    
    $("#addCalendarAccept").click(function(event){
        if (userID != "" && userEmail != "" && token != "") {
            $("#newCalendarName").hide();
            $("#addCalendar").show();
            $("#addCalendarAccept").hide();
            // Add new calendar
            var newTitle = $("#newCalendarName");
            var re = /^[a-zA-Z0-9 '&-+$!?.]*$/;
            var errorMsg = "";
            if (errorMsg == "") {
                if (newTitle.val() != "" && newTitle.val() != null) {
                    var exprEvt = newTitle.val().match(re);
                    if (exprEvt) {
                        if (newTitle.val().length > 30) {
                            errorMsg = "Event title is too long.";
                            newTitle.select();
                        }
                    }else{
                        errorMsg = "Event title contains invalid characters.";
                        newTitle.select();
                    }
                }else{
                    errorMsg = "Please enter an event title.";
                    newTitle.select();
                }
            }

            if (errorMsg == "") {
                $.post("ProcessAddCalendar.php", {"userID":userID, "token":token, "title":newTitle.val()}, function(data){
                    if (data.success == "true") {
                        retrieveCalendars();
                    }
                }, 'json');
            }
            
            retrieveCalendars();
        }
        event.preventDefault();
    });
    
    $("#addCalendar").click(function(event){
        $("#newCalendarName").show().select();
        $("#addCalendarAccept").show();
        $("#addCalendar").hide();
        event.preventDefault();
    });
    
    function displayToday() {
        currentMonth = new Month(today.getFullYear(), today.getMonth());
        generateCalendar(currentMonth);
    }
    
    function logout(){
        userID = "";
        token = "";
        userEmail = "";
        $("#logout").hide();
        $("#welcome").hide();
        $("#login").show();
        $("#register").show();
        $("#passwordLabel").show();
        $("#emailLabel").show();
        $("#emailLogin").show();
        $("#passwordLogin").show();
        $("#calendars").hide();
        $.post("Logout.php", {"command":"logout"}, function(data){}, 'json');
        generateCalendar(currentMonth);
    }
    
    function validateAddEvent(){
        if (userID != "" && userEmail != "" && token != "") {
            var errorMsg = "";
            var calendar = $("#eventCalendars");
            var hour = $("#hour");
            var minute = $("#minute");
            var title = $("#event_title");
            var cell = $("#cell");
            var date = $("#date");
            var day = $("#day");
            var month = $("#month");
            var year = $("#year");
            var details = $("#details");
            // Filter Event Title
            var re = /^[a-zA-Z0-9 '&-+$!?]*$/;
            if (errorMsg == "") {
                if (title.val() != "" && title.val() != null) {
                    var exprEvt = title.val().match(re);
                    if (exprEvt) {
                        if (title.val().length > 30) {
                            errorMsg = "Event title is too long.";
                            title.select();
                        }
                    }else{
                        errorMsg = "Event title contains invalid characters.";
                        title.select();
                    }
                }else{
                    errorMsg = "Please enter an event title.";
                    title.select();
                }
            }
            
            //Filter event details
            re = /^[a-zA-Z0-9 .?!()-+=]*$/;
            if (errorMsg == "") {
                if (details.val() != "" && details.val() != null) {
                    var exprDet = details.val().match(re);
                    if (exprDet) {
                        if (details.val().legnth > 500) {
                            errorMsg = "Event details are too long.";
                            details.select();
                        }
                    }else{
                        errorMsg = "Event details contain invalid characters.";
                        details.select();
                    }
                }
            }
            if (errorMsg != "") {
                alert(errorMsg);
            }else{
                // Send to server
                // Month offset
                var monthInt = parseInt(month.val()) + 1;
                var datetime = createDate(minute.val(), hour.val(), day.val(), monthInt, year.val());
                $.post("ProcessEventAdd.php", {"title":title.val(), "cell":cell.val(), "date":datetime,
                       "details":details.val(), "userID":userID, "email":userEmail, "token":token, "calendar_id":calendar.val()}, function(data){
                    dialog.dialog( "close" );
                    title.val("");
                    date.val("");
                    cell.val("");
                    details.val("");
                    if (data.success == "true") {
                        var eventID = data.eventID;
                        var cellID = data.cell;
                        var str = ".mainCell[id=" + cellID+"]";
                        var evtDiv = $("#calendar").find(str).first();
                        var evtDisp = $("<div class='event'></div>");
                        var titleResult = data.title;
                        if (titleResult.length >= 14) {
                            titleResult = titleResult.substring(0, 14) + "...";
                        }
                        var titleLabel = $("<label class='title'></label>").text(titleResult);
                        var timeLabel = $("<label class='time'></label>").text(hour.val() + ":" + minute.val() + $("#ampm").val());
                        var eventIDField = $("<input type='hidden'></input>").val(eventID);
                        evtDisp.append(titleLabel, timeLabel, eventIDField);
                        evtDiv.append(evtDisp);
                        
                        // If event added to a calendar that is "turned off", turn back on.
                        $.post("UpdateCalendar.php", {"userID":userID, "token":token, "calendar_id":calendar.val(), "view":"1"}, function(data){
                        }, 'json');
                    }
                    hour.val("1");
                    minute.val("00");
                    $("#ampm").val("AM");
                }, 'json');
                
                
            }
        }
    }
    
    function validateRegister() {
        var errorMsg = "";
        var re = /^[a-zA-Z0-9 ]*$/;
        var first = $("#first").val();
        var last = $("#last").val();
        var email = $("#email").val();
        var password = $("#pass").val();
        //Validate first name
        if (errorMsg == "") {
            if (first != "" && first != null) {
                var fMatch = first.match(re);
                if (fMatch) {
                    if (first.length > 30) {
                        errorMsg = "First name is too long.";
                        $("#first").select();
                    }
                }else{
                    errorMsg = "First name can only contain alphanumeric characters.";
                    $("#first").select();
                }
            }else{
                errorMsg = "Please enter a first name.";
                $("#first").select();
            }
        }
        
        //Validate last name
        if (errorMsg == "") {
            if (last != "" && last != null) {
                var lMatch = last.match(re);
                if (lMatch) {
                    if (last.length > 40) {
                        errorMsg = "Last name is too long.";
                        $("#last").select();
                    }
                }else{
                    errorMsg = "Last name can only contain alphanumeric characters.";
                    $("#last").select();
                }
            }else{
                errorMsg = "Please enter a last name.";
                $("#last").select();
            }
        }
        
        //Validate email
        re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (errorMsg == "") {
            if (email != "" && email != null) {
                var eMatch = email.match(re);
                if (eMatch) {
                    if (email.length > 50) {
                        errorMsg = "Email address is too long.";
                        $("#email").select();
                    }
                }else{
                    errorMsg = "Email address contains invalid characters.";
                    $("#email").select();
                }
            }else{
                errorMsg = "Please enter an email address.";
                $("#email").select();
            }
        }
        
        // Validate password
        re = /^[a-zA-Z0-9]*$/;
        if (errorMsg == "") {
            if (password != "" && password != null) {
                var pMatch = password.match(re);
                if (pMatch) {
                    if (password.length > 20) {
                        errorMsg = "Password is too long.";
                        $("#pass").select();
                    }
                    if (password.length < 6) {
                        errorMsg = "Password is too short.";
                        $("#pass").select();
                    }
                }else{
                    errorMsg = "Password can only contain alphanumeric characters.";
                    $("#pass").select();
                }
            }else{
                errorMsg = "Please enter a password.";
                $("#pass").select();
            }
        }
        
         if (errorMsg != "") {
            alert(errorMsg);
         }else{
            $.post("ProcessRegistration.php", {"email":email, "password":password, "first":first, "last":last}, function(value){
                if (value.success == "true") {
                    registerDialog.dialog("close");
                    // successful register and login
                    token = value.token;
                    userID = value.userID;
                    userEmail = value.email;
                    // Create default calendar.
                    var defaultCalendarName = "" + value.first + "'s Calendar";
                    $.post("ProcessAddCalendar.php", {"userID":userID, "token":token, "title":defaultCalendarName}, function(data){}, 'json');
                    $("#first").val("");
                    $("#last").val("");
                    $("#email").val("");
                    $("#pass").val("");
                    $("#login").hide();
                    $("#register").hide();
                    $("#passwordLabel").hide();
                    $("#emailLabel").hide();
                    $("#emailLogin").hide();
                    $("#passwordLogin").hide();
                    $("#welcome").text("Welcome, " + value.first);
                    $("#welcome").show();
                    $("#logout").show();
                    $("#calendars").show();

                }else{
                    alert(value.reason);
                }
            }, 'json');
            
        }
    
    }
    
    function prevMonth(){
        currentMonth = currentMonth.prevMonth();
        generateCalendar(currentMonth);
    }
    
    function nextMonth(){
        currentMonth = currentMonth.nextMonth();
        generateCalendar(currentMonth);
    }
    
   
    
    function createDate(minute, hour, day, month, year) {
        var result = ""
        var hourInt = parseInt(hour);
        if ($("#ampm").val() == "PM") {
            hourInt += 12;
        }
        hour = ""+hourInt;
        if (month.length == 1) {
            month = "0" + month;
        }
        alert
        if (day.length == 1) {
            day = "0" + day;
        }
        if (hour.length == 1) {
            hour = "0" + hour;
        }
        result = year;
        result = result.concat("-");
        result = result.concat(month);
        result = result.concat("-");
        result = result.concat(day);
        result = result.concat(" ");
        result = result.concat(hour);
        result = result.concat(":");
        result = result.concat(minute);
        result = result.concat(":00");
        return result;
    }
        
    function login(){
        //Get data field.
        var errorMsg = "";
        var email = $("#emailLogin").val();
        var password = $("#passwordLogin").val();
        // Filter email.
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (errorMsg == "") {
            if (email != "" && email != null) {
                var eMatch = email.match(re);
                if (eMatch) {
                    if (email.length > 50) {
                        errorMsg = "error.";
                    }
                    if (password.length < 6) {
                        errorMsg = "error.";
                    }
                }else{
                    errorMsg = "error.";
                }
            }else{
                errorMsg = "error.";
            }
        }
        // Filter password
        re = /^[a-zA-Z0-9]*$/;
        if (errorMsg == "") {
            if (password != "" && password != null) {
                var pMatch = password.match(re);
                if (pMatch) {
                    if (password.length > 20) {
                        errorMsg = "error.";
                    }
                    if (password.length < 6) {
                        errorMsg = "error.";
                    }
                }else{
                    errorMsg = "error.";
                }
            }else{
                errorMsg = "error.";
            }
        }

        if (errorMsg == "") {
            // attempt login.
            $.post("ProcessLogin.php", {"email":email, "password":password}, function(data){
            if (data.status == "true") {
                token = data.token;
                userEmail = data.email;
                userID = data.userID;
                loadEvents();
                $("#emailLogin").val("");
                $("#passwordLogin").val("");
                $("#login").hide();
                $("#register").hide();
                $("#passwordLabel").hide();
                $("#emailLabel").hide();
                $("#emailLogin").hide();
                $("#passwordLogin").hide();
                $("#welcome").text("Welcome, " + data.first);
                $("#welcome").show();
                $("#logout").show();
                $("#calendars").show();
            }
            
            
        }, 'json');
        }
        
        $("#emailLogin").val("");
        $("#passwordLogin").val("");
    }

    function eventHandle(event) {
        // Check user login
        if (userID != "" && userEmail !="" && token !="") {
            // Clicked on empty space
            if (event.target == this){
                // Add new event
                var day = $(this).children().first().text();
                var month = $(this).children("input").first().val();
                var year = $(this).children("input").first().attr("name");
                if (month < currentMonth.month) {
                    if (year > currentMonth.year) {
                        var m = currentMonth.nextMonth();
                    }else{
                        var m = currentMonth.prevMonth();
                    }
                }else if (month > currentMonth.month) {
                    if (year < currentMonth.year) {
                        var m = currentMonth.prevMonth();
                    }else{
                        var m = currentMonth.nextMonth();
                    }
                }else{
                    var m = currentMonth;
                }
                var date = new Date(m.year, m.month, day);
                
                $("#day").val(day);
                $("#month").val(month);
                $("#year").val(year);
                $("#date").val(date.toDateString());
                $("#cell").val($(this).attr("id"));
                
                
                dialog.dialog( "option", "buttons", [ {text: "Add Event", click: validateAddEvent}, 
                                    { text: "Cancel", click: function() {
                                        $( this ).dialog( "close" );
                                        $("#hour").val("1");
                                        $("#minute").val("00");
                                        $("#ampm").val("AM");
                                        $("#eventID").val("");
                                        $("#details").val("");
                                        $("#event_title").val("");
                                    } } ] );
                
                var eventCalendars = $("#eventCalendars");
                $.post("RetrieveCalendars.php", {"userID":userID, "token":token}, function(data){
                    if (data.success == "true") {
                        eventCalendars.empty();
                        for (var j=0; j<Object.keys(data).length-1; j++){
    
                            var calendarID = data[j]['calendar_id'];
                            var title = data[j]['title'];
                            // create calendar view
                            var calendarOption = $("<option></option>").val(calendarID).text(title);
                            eventCalendars.append(calendarOption);
                        }
                    }
                }, 'json');
                dialog.dialog( "open" );
            }else{
                // Open event edit dialog.
                if (event.target.nodeName == "DIV") {
                    var target = $(event.target);
                }else{
                    var target = $(event.target).parent();
                }
                
                var eventID = parseInt(target.children("input").first().val());
                $.post("RetrieveASingleEvent.php", {"userID":userID, "token":token, "eventID":eventID}, function(data){
                    var eventTitle = data.title;
                    var eventCalendarID = data.calendar_id;
                    var eventDate = data.date;
                    var eventDetails = data.details;
                    var eventDate = parseDate(eventDate);
                    var eventYear = eventDate[0];
                    var eventMonth = eventDate[1];
                    var eventDay = eventDate[2];
                    var eventHour = parseInt(eventDate[3]);
                    var eventMinute = eventDate[4];

                    var date = new Date(eventYear, eventMonth, eventDay);
                    $("#event_title").val(eventTitle);
                    if (eventHour > 12) {
                        eventHour -= 12;
                        $("#ampm").val("PM");
                    }else{
                        $("#ampm").val("AM");
                    }
                    $("#details").val(eventDetails);
                    $("#hour").val(eventHour);
                    $("#minute").val(eventMinute);
                    $("#day").val(eventDay);
                    $("#month").val(eventMonth);
                    $("#year").val(eventYear);
                    $("#date").val(date.toDateString());
                    $("#cell").val("" + eventYear + eventMonth + eventDay);
                    $("#eventID").val(eventID);
                    var eventCalendars = $("#eventCalendars");
                    $.post("RetrieveCalendars.php", {"userID":userID, "token":token}, function(data){
                        if (data.success == "true") {
                            eventCalendars.empty();
                            for (var j=0; j<Object.keys(data).length-1; j++){
                                var calendarID = data[j]['calendar_id'];
                                var title = data[j]['title'];
                                // create calendar view
                                var calendarOption = $("<option></option>").val(calendarID).text(title);
                                eventCalendars.append(calendarOption);
                            }
                            eventCalendars.val(eventCalendarID);
                        }
                        
                    }, 'json');
                    dialog.dialog( "option", "buttons", [ {text: "Ok", click: validateEditEvent},{text: "Delete", click: deleteEvent},
                                    { text: "Cancel", click: function() {
                                        $( this ).dialog( "close" );
                                        $("#hour").val("1");
                                        $("#minute").val("00");
                                        $("#ampm").val("AM");
                                        $("#eventID").val("");
                                        $("#details").val("");
                                        $("#event_title").val("");
                                        } } ] );
                    
                    
                    dialog.dialog( "open" );
                    }, 'json');
            }
        }
    }
   
    
    function calendarInit(){
        var table = $("#calendar");
        table.empty();
        var prevMonth = $("<button id='prevMonth'></button>").text("<").css({"float":"right"});
        var nextMonth = $("<button id='nextMonth'></button>").text(">").css({"float":"left"});
        var topRow = $("<tr></tr").css({"border":"none", "align":"center"});
        var headCell = $("<td></td>").text(monthStrings[currentMonth.month]).css({"border":"none",
                "vertical-align":"bottom", "height":".5em", "margin-left":"auto","margin-right":"auto",
                "text-align":"center", "font-size":"19pt", "align":"center"});
        var leftCell = $("<td></td>").attr("colspan", "3").css({"vertical-align":"bottom", "border":"none", "height":".5em"});
        var rightCell = $("<td></td>").attr("colspan","3").css({"vertical-align":"bottom", "border":"none","height":".5em"});
        var todayButton = $("<button id='today'></button>").text("Today").css({"float":"left", "background-color":"DarkCyan", "color":"white"});
        var calendarButton = $("<button id='calendars'></button>").text("Calendars").css({"float":"left", "background-color":"DarkCyan", "color":"white"}).hide()
        leftCell.append(prevMonth);
        rightCell.append(nextMonth);
        leftCell.append(todayButton);
        leftCell.append(calendarButton);
        topRow.append(leftCell, headCell, rightCell);

        table.append(topRow);      
        var headerRow = "<tr><th>Sunday</th><th>Monday</th><th>Tuesday</th>" +
        "<th>Wednesday</th><th>Thursday</th><th>Friday</th><th>Saturday</th></tr>";
        table.append(headerRow);
        for (var i=0; i<5; i++){
            addRow();
        }

        generateCalendar(currentMonth);
    }
    
    function addRow(){
        var table = $("#calendar");
        var weekDisplay = $("<tr></tr>");
        for (var j=0; j<7; j++){
            var dayDisplay = $("<td class='mainCell'></td>").click(eventHandle);
            var dayHeader = $("<th></th>");
            var inCurrMonth = $("<input type='hidden'></input>");
            dayDisplay.append(dayHeader);
            dayDisplay.append(inCurrMonth);
            weekDisplay.append(dayDisplay);
        }
        table.append(weekDisplay);
    }
    
    function removeRow(){
        var table = $("#calendar");
        table.children().children().last().remove();
    }
    
    function generateCalendar(month){
        // Update enabled Calendar array.
        var table = $("#calendar");
        $(".event").remove();     
        var monthDisp = table.children().children().first().children().eq(1).text(monthStrings[currentMonth.month] + " " +currentMonth.year);
        var weeks = month.getWeeks();
        if(weeks.length == 6) {
            if (table.children().children().length <= 7) {
                //draw extra row
                addRow();
                $("td.mainCell").css("height", "5em");
            }
        }else if (weeks.length == 5){
            if (table.children().children().length == 8) {
                //remove extra row
                removeRow();
                $("td.mainCell").css("height", "6em");

            }else if (table.children().children().length == 6) {
                //draw extra row
                addRow();
                $("td.mainCell").css("height", "6em");
            }
        }else if (weeks.length == 4) {
            removeRow();
            $("td.mainCell").css("height", "7em");
        }
        var currentRow = table.children().children().first().next();
        for (var i=0; i<weeks.length; i++){
            currentRow = currentRow.next();
            var currentCell = currentRow.children().first();
            var days = weeks[i].getDates();
            for (var j=0; j<days.length; j++){
                var dayHeader = currentCell.children().first();
                var date = days[j].getDate();
                dayHeader.text(date);
                var cellId = ""+days[j].getFullYear()+days[j].getMonth()+days[j].getDate();
                if (cellId == "" +today.getFullYear()+today.getMonth()+today.getDate()){
                    currentCell.css({"border-color":"DarkCyan", "border-width":"medium"});
                }else{
                    currentCell.css({"border-width":"thin","border-color":"black"});
                }
                currentCell.attr("id", cellId);
                var inCurrMonth = dayHeader.next();
                inCurrMonth.attr("name", days[j].getFullYear());
                inCurrMonth.val(days[j].getMonth());
                if (days[j].getMonth() != currentMonth.month) {
                    // next month
                    dayHeader.css("color", "LightGray");
                }else{
                    dayHeader.css("color", "black");
                }
                currentCell = currentCell.next();
            }
        }
        loadEvents();
    }

});

