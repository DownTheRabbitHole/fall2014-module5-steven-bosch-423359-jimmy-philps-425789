<?php
    require 'DatabaseConn.php';
    require 'RandomString.php';

    header('Content-Type: application/json');
    ini_set("session.cookie_httponly", 1);
    session_name("newSession");
    session_start();

    $errorMsg = "";
    // Ensure user is logged in and valid.
    if (isset($_POST['userID'])){
        $userID = (int)$_POST['userID'];
        if (empty($userID)){
           $errorMsg .= "No user."; 
        }
        if ($userID != $_SESSION['userID']){
            $errorMsg .= "Bad user";
        }
    }else{
        $errorMsg .= "No user.";
    }
    
    if (isset($_POST['token'])){
        $token = (string)$_POST['token'];
        if (empty($token)){
           $errorMsg .= "No token"; 
        }
        if ($token != $_SESSION['token']){
            $errorMsg .= "Bad token";
        }
    }else{
        $errorMsg .= "No token.";
    }
    
    if (isset($_POST['eventID'])){ // shouldnt be necessary
        $eventID = (string) trim($_POST['eventID']);
        if (empty($eventID)){
            $errorMsg .= "Please provide an event id1";
        }
    }else{
        $errorMsg .= "Please provide an event id2";
    }
    
    $arr = array("success" => $errorMsg);
    
    if (empty($errorMsg)){
        // Delete event.
        $stmt = $mysqli->prepare("delete from Module5.event where id=?");
        if(!$stmt){
            echo json_encode($arr);
            printf("Query Prep Failed: %s<br>", $mysqli->error);
            exit;
        }
        $stmt->bind_param('i', $eventID); // Do not have all of these yet. sssss is not correct
        $stmt->execute();
        $stmt->close();
        
        $arr = array("success" => "true");
        
    }   
    
    echo json_encode($arr);
    exit;
?>