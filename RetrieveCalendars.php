<?php
    require 'DatabaseConn.php';

    header('Content-Type: application/json');
    ini_set("session.cookie_httponly", 1);
    session_name("newSession");
    session_start();

    $errorMsg = "";
    
    // Ensure user is logged in and valid.
    if (isset($_POST['userID'])){
        $userID = (int)$_POST['userID'];
        if (empty($userID)){
           $errorMsg .= "No user."; 
        }
        if ($userID != $_SESSION['userID']){
            $errorMsg .= "Bad user";
        }
    }else{
        $errorMsg .= "No user.";
    }
    
    if (isset($_POST['token'])){
        $token = (string)$_POST['token'];
        if (empty($token)){
           $errorMsg .= "No token"; 
        }
        if ($token != $_SESSION['token']){
            $errorMsg .= "Bad token";
        }
    }else{
        $errorMsg .= "No token.";
    }
    
        
    $arrArr = array("success" => $errorMsg);
    $arr = [];
    $count = 0;
    if (empty($errorMsg)){
        
        // Request events.
        $stmt = $mysqli->prepare("select id, title, view from Module5.calendar where user_id=?");
        if (!$stmt){
            echo json_encode($arr);
            exit;
        }
        $stmt->bind_param('i', $userID);
        $stmt->execute();
        $stmt->bind_result($id, $title, $view);
        while ($stmt->fetch()){
            $arr = array("title"=>$title, "calendar_id"=>$id, "view"=>$view);
            $arrArr[$count] = $arr;
            $count++;
        }
        
         
        $stmt->close();
        $arrArr['success'] = "true";
    }
    echo json_encode($arrArr);
    exit;
    
    
    
?>