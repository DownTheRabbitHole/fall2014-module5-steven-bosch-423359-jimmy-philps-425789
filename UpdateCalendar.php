<?php
    require 'DatabaseConn.php';

    header('Content-Type: application/json');
    ini_set("session.cookie_httponly", 1);
    session_name("newSession");
    session_start();

    $errorMsg = "";
    
    // Ensure user is logged in and valid.
    if (isset($_POST['userID'])){
        $userID = (int)$_POST['userID'];
        if (empty($userID)){
           $errorMsg .= "No user."; 
        }
        if ($userID != $_SESSION['userID']){
            $errorMsg .= "Bad user";
        }
    }else{
        $errorMsg .= "No user.";
    }
    
    if (isset($_POST['token'])){
        $token = (string)$_POST['token'];
        if (empty($token)){
           $errorMsg .= "No token"; 
        }
        if ($token != $_SESSION['token']){
            $errorMsg .= "Bad token";
        }
    }else{
        $errorMsg .= "No token.";
    }
    
    if (isset($_POST['calendar_id'])){
        $calendar_id = (int)$_POST['calendar_id'];
        if (empty($calendar_id)){
            $errorMsg .= "No calendar id.";
        }
    }else{
        $errorMsg .= "No calendar id.";
    }
    
    if (isset($_POST['view'])){
        $view = (int)$_POST['view'];
    }else{
        $errorMsg .= "No view";
    }
    
        
    $arr = array("success" => $errorMsg, "view"=>$view, "id"=>$calendar_id);
    if (empty($errorMsg)){
        // update calendar
        
        $stmt = $mysqli->prepare("update Module5.calendar set view=? where id=?");
        if (!$stmt){
            echo json_encode($arr);
            exit;
        }
        $stmt->bind_param('ii', $view, $calendar_id);
        $stmt->execute();    
        $stmt->close();
        $arr = array("success"=>"true", "view"=>$view, "id"=>$calendar_id);
    }
    echo json_encode($arr);
    exit;
    
    
    
?>