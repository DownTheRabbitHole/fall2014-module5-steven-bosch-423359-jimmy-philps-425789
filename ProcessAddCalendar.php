<?php
    require 'DatabaseConn.php';

    header('Content-Type: application/json');
    ini_set("session.cookie_httponly", 1);
    session_name("newSession");
    session_start();

    $errorMsg = "";
    
    // Ensure user is logged in and valid.
    if (isset($_POST['userID'])){
        $userID = (int)$_POST['userID'];
        if (empty($userID)){
           $errorMsg .= "No user."; 
        }
        if ($userID != $_SESSION['userID']){
            $errorMsg .= "Bad user";
        }
    }else{
        $errorMsg .= "No user.";
    }
    
    if (isset($_POST['token'])){
        $token = (string)$_POST['token'];
        if (empty($token)){
           $errorMsg .= "No token"; 
        }
        if ($token != $_SESSION['token']){
            $errorMsg .= "Bad token";
        }
    }else{
        $errorMsg .= "No token.";
    }
    
    if (isset($_POST['title'])){
        $title = (string)$_POST['title'];
        if (empty($title)){
            $errorMsg .= "No title.";
        }
        if (!preg_match("/^[a-zA-Z0-9 '&-+$!?.]*$/",$title)) {
            $errorMsg .= "Event details contain illegal characters.";
        }
    }
    
    $arr = array("success" => $errorMsg);
    if (empty($errorMsg)){
        // Insert calendar.
        $stmt = $mysqli->prepare("insert into Module5.calendar (title, user_id) values (?,?)");
        if (!$stmt){
            echo json_encode($arr);
            exit;
        }
        $stmt->bind_param('si', $title, $userID);
        $stmt->execute();
        $stmt->close();

        $arr = array("success"=>"true");
           
    }
    echo json_encode($arr);
    exit;
    
    
    
?>