<?php
    require 'DatabaseConn.php';

    header('Content-Type: application/json');
    ini_set("session.cookie_httponly", 1);
    session_name("newSession");
    session_start();

    $errorMsg = "";
    
    // Ensure user is logged in and valid.
    if (isset($_POST['userID'])){
        $userID = (int)$_POST['userID'];
        if (empty($userID)){
           $errorMsg .= "No user."; 
        }
        if ($userID != $_SESSION['userID']){
            $errorMsg .= "Bad user";
        }
    }else{
        $errorMsg .= "No user.";
    }
    
    if (isset($_POST['token'])){
        $token = (string)$_POST['token'];
        if (empty($token)){
           $errorMsg .= "No token"; 
        }
        if ($token != $_SESSION['token']){
            $errorMsg .= "Bad token";
        }
    }else{
        $errorMsg .= "No token.";
    }
    
    if (isset($_POST['eventID'])){
        $id = (int)$_POST['eventID'];
        if (empty($id)){
            $errorMsg .= "No id.";
        }
    }
    
    $arr = array("success" => $errorMsg);
    
    if (empty($errorMsg)){
        
        // Request events.
        $stmt = $mysqli->prepare("select title, date, details, calendar_id from Module5.event where user_id=? and id=?");
        if (!$stmt){
            echo json_encode($arr);
            exit;
        }
        $stmt->bind_param('ii', $userID, $id);
        $stmt->execute();
        $stmt->bind_result($title, $date, $details, $calendar_id);
        $stmt->fetch();
        $stmt->close();

        $arr = array("success"=>"true", "title"=>$title, "date"=>$date, "details"=>$details, "calendar_id"=>$calendar_id);
           
    }
    echo json_encode($arr);
    exit;
    
    
    
?>