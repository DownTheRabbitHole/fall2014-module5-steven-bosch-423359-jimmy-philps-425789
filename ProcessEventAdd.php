<?php
    require 'DatabaseConn.php';

    header('Content-Type: application/json');
    ini_set("session.cookie_httponly", 1);
    session_name("newSession");
    session_start();

    $errorMsg = "";
    
    // Ensure user is logged in and valid.
    if (isset($_POST['userID'])){
        $userID = (int)$_POST['userID'];
        if (empty($userID)){
           $errorMsg .= "No user."; 
        }
        if ($userID != $_SESSION['userID']){
            $errorMsg .= "Bad user";
        }
    }else{
        $errorMsg .= "No user.";
    }
    
    if (isset($_POST['token'])){
        $token = (string)$_POST['token'];
        if (empty($token)){
           $errorMsg .= "No token"; 
        }
        if ($token != $_SESSION['token']){
            $errorMsg .= "Bad token";
        }
    }else{
        $errorMsg .= "No token.";
    }
    
    if (isset($_POST['email'])){
        $email = (string) $_POST['email'];
        if (empty($email)){
           $errorMsg .= "No email.";  
        }
    }else{
        $errorMsg .= "No email.";
    }
    

    if (isset($_POST['cell'])){
        $cell = (string)$_POST['cell'];
    }else{
        $errorMsg .= "No cell selected.";
    }
    
    if (isset($_POST['date'])){
        $date = (string)$_POST['date'];
    }else{
        $errorMsg .= "No date selected.";
    }
    
    // Validate event details.
    if (isset($_POST['details'])){
        $details = (string) trim($_POST['details']);
        if (!empty($details)){
            if (!preg_match("/^[a-zA-Z0-9 .?!()-+=]*$/",$details)) {
                $errorMsg .= "Event details contain illegal characters.";
            }
            
            if (strlen($details) > 500){
                $errorMsg .= "Event details are too long.<br>";
            }
        }
    }
    
    //Validate event title.
    if (isset($_POST['title'])){
        $title = (string) trim($_POST['title']);
        if (!empty($title)){
            
            if (!preg_match("/^[a-zA-Z0-9 '&-+$!?]*$/",$title)) {
                $errorMsg .= "Event title must contain letters and numbers only.<br>";
            }
            
            if (strlen($title) > 30){
                $errorMsg .= "Event Title is too long.<br>";
            }
        }else{
          $errorMsg .= "Please provide an event title1.<br>";
        }
    }else{
        $errorMsg .= "Please provide an event title2.<br>";
    }
    
    if (isset($_POST['calendar_id'])){
        $calendar_id = (int)$_POST['calendar_id'];
        if (empty($calendar_id)){
            $errorMsg .= "no calendar id";
        }
    }else{
        $errorMsg .= "no calendar id.";
    }
    
    $arr = array("success" => $errorMsg);

    
    if (empty($errorMsg)){
        
        // Send event.
        $stmt = $mysqli->prepare("insert into Module5.event (date, title, user_id, calendar_id, details) values (?,?,?,?,?)");
        if(!$stmt){
            echo json_encode($arr);
            exit;
        }
        
        $stmt->bind_param('ssiis', $date, $title, $userID, $calendar_id, $details);
        $stmt->execute();
        $stmt->close();
        
        $stmt = $mysqli->prepare("select id from Module5.event order by id desc limit 0,1");
        $stmt->execute();
        $stmt->bind_result($id);
        $stmt->fetch();
        $stmt->close();
        $arr = array("success"=>"true", "title"=>$title, "cell"=>$cell, "date"=>$date, "eventID"=>$id);
        
        
        
    }
    echo json_encode($arr);
    exit;
    
    
    
?>