<?php
    require 'DatabaseConn.php';
    require 'RandomString.php';

    header('Content-Type: application/json');
    
    $errorMsg = "";
    //Validate first name.
    if (isset($_POST['first'])){
        $firstname = (string) trim($_POST['first']);
        if (!empty($firstname)){
            if (!preg_match("/^[a-zA-Z ]*$/",$firstname)) {
                $errorMsg .= "First name must contain letters only.<br>";
            }
            if (strlen($firstname) > 30){
                $errorMsg .= "First name is too long.<br>";
            }
        }else{
          $errorMsg .= "Please provide a first name.<br>";
        }
    }else{
        $errorMsg .= "Please provide a first name.<br>";
    }
    
    //Validate last name.
    if (isset($_POST['last'])){
        $lastname = (string) trim($_POST['last']);
        if (!empty($lastname)){
            if (!preg_match("/^[a-zA-Z ]*$/",$lastname)) {
                $errorMsg .= "Last name must contain letters only.<br>";
            }
            if (strlen($lastname) > 40){
                $errorMsg .= "Last name is too long.<br>";
            }
        }else{
            $errorMsg .= "Please provide a last name.<br>";
        }
    }else{
        $errorMsg .= "Please provide a last name.<br>";
    }
    
    //Validate email.
    if (isset($_POST['email'])){
        $email = (string) trim($_POST['email']);
        if (!empty($email)){
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $errorMsg .= "Invalid email address format.<br>";
            }
            if (strlen($email) > 250){
                $errorMsg .= "Email address is too long.<br>";
            }
            
             // Ensure unique email.
             
            $stmt = $mysqli->prepare("select email from Module5.user");
            if(!$stmt){
                printf("Query Prep Failed: %s<br>", $mysqli->error);
                exit;
            }
            $stmt->execute();
            $stmt->bind_result($existing_email);
            while ($stmt->fetch()){
                if ($email == $existing_email){
                    $errorMsg .= "An account with the email you provided already exists.";
                }
            }
            $stmt->close();
        }else{
            $errorMsg .= "Please provide an email address.<br>";
        }
    }else{
        $errorMsg .= "Please provide an email address.<br>";
    }
    
    //Validate inputted password.
    if (isset($_POST['password']) && !empty($_POST['password'])){
        $password = (string) $_POST['password'];
        if (!preg_match("/^[a-zA-Z0-9]*$/",$password)) {
            $errorMsg .= "Password must contain only letters or numbers.<br>";
        }
        if (strlen($password) < 6){
            $errorMsg .= "Password must be at least 6 characters.<br>";
        }
        if (strlen($password) > 30){
            $errorMsg .= "Password is too long.<br>";
        }
    }else{
        $errorMsg .= "Please enter a password.<br>";
    }
    
    $arr = array("success" => false);
    if (empty($errorMsg)){
        if (isset($_SESSION['error'])){
            unset($_SESSION['error']);
        }
        
        // Encrypt password.
        $randString = randomString(22);
        $salt = "$2y$07$".$randString;
        
      
        $password_hash = crypt ($password, $salt);
    
        // Register user.
        $stmt = $mysqli->prepare("insert into Module5.user (first_name, last_name, email, password_hash) values (?,?,?,?)");
        if(!$stmt){
            printf("Query Prep Failed: %s<br>", $mysqli->error);
            exit;
        }
        $stmt->bind_param('ssss', $firstname, $lastname, $email, $password_hash);
        $stmt->execute();
        $stmt->close();
        
        // Obtain new user's ID.
        $stmt = $mysqli->prepare("select id from Module5.user where email=?");
        if(!$stmt){
            printf("Query Prep Failed: %s<br>", $mysqli->error);
            echo json_encode($arr);
            exit;
        } 
        $stmt->bind_param('s', $email);
        $stmt->execute();
        $stmt->bind_result($id);
        $stmt->fetch();
         
        
        // Gerate session token
        $token = randomString(10);
        ini_set("session.cookie_httponly", 1);
        session_name("newSession");
        session_start();
        $_SESSION['token'] = $token;
        $_SESSION['userID'] = $id;
        
        
        $arr = array("success" => "true", "first"=>$firstname, "email"=>$email, "token"=>$token, "userID"=>$id);
    }else{
        $arr = array("success" => "false", "reason" => $errorMsg);
    }
    echo json_encode($arr);
    exit;
?>