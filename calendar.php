<!DOCTYPE html>

<html>
<head>
    <title>Your Calendar</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css"/>
    <link rel="stylesheet" type="text/css" href="calendar.css"/>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js" type="text/javascript" ></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script src="calendarmin.js" type="text/javascript"></script>
    <script src="calendar.js" type="text/javascript"></script>
    
</head>

<body>
<label for="emailLogin" id="emailLabel">Email: </label>
<input type="text" name="email" id="emailLogin"/>
<label for="passwordLogin" id="passwordLabel">Password: </label>
<input type="password" name="password" id="passwordLogin"/><br>
<input type="button" value="Login" id="login"/>
<input type="button" value="Register" id="register"/>
<label id="welcome"></label><br>
<input type="button" value="Logout" id="logout"/>
<!-- Calendar -->
<table id="calendar"></table>

<!-- Add/Edit Event Dialog -->
<div id="dialog-form" title="Event">
  <form>
    <fieldset>
        <label for="eventCalendars">Calendar:</label><br>
        <select id="eventCalendars">
        </select><br><br>
        <label for="event_title">Event:</label>
        <input type="text" name="event_title" id="event_title" class="text ui-widget-content ui-corner-all"/>
        <label for="date">Date:</label>
        <input type="text" id="date" class="text ui-widget-content ui-corner-all" disabled="disabled"/> 
        <label>Time:</label><br>
        <select id="hour">
        <?php
            for($hours=1; $hours<=12; $hours++){
                echo "<option>".$hours."</option>";
            }
        ?>
        </select>:
        <select id="minute">
        <?php
            for($mins=0; $mins<60; $mins+=1){
                echo '<option>'.str_pad($mins,2,'0',STR_PAD_LEFT).'</option>';
            }
        ?>
        </select>
        <select id="ampm">
            <option>AM</option>
            <option>PM</option>
        </select><br><br>
        <label for="details">Details:</label><br>
        <textarea id="details"></textarea>
        <input type="hidden" id="cell"/>
        <input type="hidden" id="day"/>
        <input type="hidden" id="month"/>
        <input type="hidden" id="year"/>
        <input type="hidden" id="eventID"/>

    </fieldset>
  </form>
</div>



<div id="dialog-register" title="Register">
    <form>
        <fieldset>
            <label>Hi there! Please register below to make your own calendar.</label><br><br>
            <label for="first">First Name:</label>
            <input type="text" id="first"/><br><br>
            <label for="last">Last Name:</label>
            <input type="text" id="last"/><br><br>
            <label for="email">Email Address:</label>
            <input type="text" id="email"/><br><br>
            <label for="pass">Password:</label><br>
            <input type="password" id="pass"/>
        </fieldset>
    </form>
</div>

<div id="dialog-calendars" title="Calendars">
    <form>
        <fieldset>
            <label>Select which calendars you'd like to view.</label><br><br>
            
            <button id="addCalendar">Add Calendar</button>
            <input type="text" id="newCalendarName" value="New Calendar">
            <button id="addCalendarAccept">Add</button><br><br>
            
            <label class="lblCal">Calendars:</label>
                <label class="checkCal">View?</label><br><hr><br>
            <div id="calendarDiv"></div>
        </fieldset>
    </form>
</div>



</body>
</html>
