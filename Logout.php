<?php
header('Content-Type: application/json');
session_start();
//Destory the session.
unset($_SESSION['userID']);
unset($_SESSION['token']);
session_destroy();
$arr = array("success"=>"true");
echo json_encode($arr);
exit;
?>