<?php
    require 'DatabaseConn.php';
    require 'RandomString.php';
    
    header('Content-Type: application/json');
    
    
    // Ensure proper post fields have been submitted.
    $arr = array("status" => false);
    if (isset($_POST['email']) && !empty($_POST['email']) && isset($_POST['password']) && !empty($_POST['password'])){
        $email = (string) $_POST['email'];
        $password = (string) $_POST['password'];
        // Filter email and password.
        if (strlen($email) < 50 && strlen($password) < 20){
            if (filter_var($email, FILTER_VALIDATE_EMAIL) && preg_match("/^[a-zA-Z0-9]*$/", $password)) {
                // Check for correct password.
                $stmt = $mysqli->prepare("select id, email, password_hash, first_name from user");
                if(!$stmt){
                    printf("Query Prep Failed: %s\n", $mysqli->error);
                    exit;
                }
                $stmt->execute();
                $stmt->bind_result($id, $existing_email, $existing_hash, $first);
                while ($stmt->fetch()){
                    if ($email == $existing_email){
                        // Match!
                        if ($existing_hash == substr(crypt($password, $existing_hash),0,50)){
                            // User verified.
                            // Set session token.
                            $token = randomString(10);
                            ini_set("session.cookie_httponly", 1);
                            session_name("newSession");
                            session_start();
                            $_SESSION['token'] = $token;
                            $_SESSION['userID'] = $id;
                            $arr = array("status" => "true", "email" => $email, "userID" => $id, "token"=>$token, "first"=>$first);
                            break;
                        }
                    }
                }
                $stmt->close();
            }  
        }
    }
    
    echo json_encode($arr);
    exit;
    

?>